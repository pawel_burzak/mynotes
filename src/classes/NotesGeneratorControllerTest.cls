/*
 * Created by Burzak on 2018-05-10.
 */

@IsTest
private class NotesGeneratorControllerTest {
    @IsTest
    static void whenValidDataThenInsertRecordAndReturnSamePage() {
        //All fields are filled
        MyNoteBuilder builder = new MyNoteBuilder();
        My_Note__c testMyNote = builder.build().getMyNote();

        NotesGeneratorController notesGeneratorController = createControllerData(testMyNote);

        List<My_Note__c> testDBMyNotes = [
                Select Id, Title__c, Description__c, Keywords__c, Effective_Date__c, Active__c
                from My_Note__c
                where Title__c = 'Test title'
        ];

        testMyNote.Id = testDBMyNotes.get(0).Id;

        System.assertEquals('Success! Your Note has been created.', ApexPages.getMessages()[0].getSummary());
        System.assertEquals(ApexPages.Severity.CONFIRM, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals(testMyNote, testDBMyNotes.get(0));
        System.assertEquals(null, notesGeneratorController.getMyNote().Title__c);
        System.assertEquals(null, notesGeneratorController.getMyNote().Description__c);
        System.assertEquals(null, notesGeneratorController.getMyNote().Keywords__c);
        System.assertEquals(null, notesGeneratorController.getMyNote().Effective_Date__c);
        System.assertEquals(false, notesGeneratorController.getMyNote().Active__c);

        //Only required fields are filled
        testMyNote = builder.effectiveDate(null).active(null).build().getMyNote();

        notesGeneratorController = createControllerData(testMyNote);

        System.assertEquals('Success! Your Note has been created.', ApexPages.getMessages()[0].getSummary());
        System.assertEquals(ApexPages.Severity.CONFIRM, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals(null, notesGeneratorController.getMyNote().Title__c);
        System.assertEquals(null, notesGeneratorController.getMyNote().Description__c);
        System.assertEquals(null, notesGeneratorController.getMyNote().Keywords__c);
        System.assertEquals(null, notesGeneratorController.getMyNote().Effective_Date__c);
        System.assertEquals(false, notesGeneratorController.getMyNote().Active__c);
    }

    @IsTest
    static void whenInvalidDataThenAddMessagesAndReturnSamePageWithInputFields() {
        //Required title field is not filled
        MyNoteBuilder builder = new MyNoteBuilder();
        My_Note__c testMyNote = builder.title(null).build().getMyNote();

        NotesGeneratorController notesGeneratorController = createControllerData(testMyNote);

        List<My_Note__c> testDBMyNotes = [
                Select Id, Title__c, Description__c, Keywords__c, Effective_Date__c, Active__c
                from My_Note__c
                where Title__c = 'Test title'
        ];

        System.assertEquals('Required fields are missing: [Title__c]', ApexPages.getMessages()[0].getSummary());
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals(0, testDBMyNotes.size());
        System.assertEquals(null, notesGeneratorController.getMyNote().Title__c);
        System.assertEquals(testMyNote.Description__c, notesGeneratorController.getMyNote().Description__c);
        System.assertEquals(testMyNote.Keywords__c, notesGeneratorController.getMyNote().Keywords__c);
        System.assertEquals(testMyNote.Effective_Date__c, notesGeneratorController.getMyNote().Effective_Date__c);
        System.assertEquals(testMyNote.Active__c, notesGeneratorController.getMyNote().Active__c);

        //Required title and description fields are not filled
        testMyNote = builder.title(null).description(null).build().getMyNote();

        notesGeneratorController = createControllerData(testMyNote);

        System.assertEquals('Required fields are missing: [Title__c]', ApexPages.getMessages()[0].getSummary());
        System.assertEquals('Field Description is required!', ApexPages.getMessages()[1].getSummary());
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[0].getSeverity());
        System.assertEquals(ApexPages.Severity.ERROR, ApexPages.getMessages()[1].getSeverity());
        System.assertEquals(null, notesGeneratorController.getMyNote().Title__c);
        System.assertEquals(null, notesGeneratorController.getMyNote().Description__c);
        System.assertEquals(testMyNote.Keywords__c, notesGeneratorController.getMyNote().Keywords__c);
        System.assertEquals(testMyNote.Effective_Date__c, notesGeneratorController.getMyNote().Effective_Date__c);
        System.assertEquals(testMyNote.Active__c, notesGeneratorController.getMyNote().Active__c);
    }

    //Helper
    private static NotesGeneratorController createControllerData(My_Note__c myNote) {
        NotesGeneratorController notesGeneratorController = new NotesGeneratorController();
        notesGeneratorController.getMyNote().Title__c = myNote.Title__c;
        notesGeneratorController.getMyNote().Description__c = myNote.Description__c;
        notesGeneratorController.getMyNote().Keywords__c = myNote.Keywords__c;
        notesGeneratorController.getMyNote().Effective_Date__c = myNote.Effective_Date__c;
        notesGeneratorController.getMyNote().Active__c = myNote.Active__c;
        notesGeneratorController.save();
        return notesGeneratorController;
    }

}