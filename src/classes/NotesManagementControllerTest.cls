/*
 * Created by Burzak on 2018-04-30.
 */

@IsTest
private class NotesManagementControllerTest {
    @TestSetup
    static void setup() {
        MyNoteBuilder builder = new MyNoteBuilder();
        builder.save();
    }

    @IsTest
    static void testGetMyNotes() {
        NotesManagementController notesManagementController = new NotesManagementController();

        //When StandardSetController is null
        List<My_Note__c> myNotes = notesManagementController.myNotes;
        System.assertEquals(true, myNotes.isEmpty());

        //When StandardSetController is not null
        setStandardSetController(notesManagementController);

        myNotes = notesManagementController.myNotes;
        System.assertEquals(1, myNotes.size());
    }

    @IsTest
    static void testGetTotalRecord() {
        NotesManagementController notesManagementController = new NotesManagementController();

        //When StandardSetController is null
        Integer totalRecord = notesManagementController.totalRecord;
        System.assertEquals(0, totalRecord);

        //When StandardSetController is not null
        setStandardSetController(notesManagementController);

        totalRecord = notesManagementController.totalRecord;
        System.assertEquals(1, totalRecord);
    }

    @IsTest
    static void testGetPageSize() {
        NotesManagementController notesManagementController = new NotesManagementController();

        //When StandardSetController is null
        String pageSize = notesManagementController.pageSize;
        System.assertEquals('0-0 of 0', pageSize);

        //When StandardSetController is not null
        setStandardSetController(notesManagementController);

        pageSize = notesManagementController.pageSize;
        System.assertEquals('1-1 of 1', pageSize);
    }

    @IsTest
    static void testSearchNoteWhenAllFieldsAreFilled() {
        NotesManagementController notesManagementController = new NotesManagementController();

        //One word in Keywords is filled
        notesManagementController.title = 'Test title';
        notesManagementController.keywords = 'Test keywords';
        notesManagementController.effectiveDate = date.valueOf('2018-01-01');
        notesManagementController.active = false;

        notesManagementController.searchNote();

        System.assertEquals(1, notesManagementController.con.getRecords().size());
        System.assertEquals(10, notesManagementController.con.getPageSize());
        System.assertEquals(1, notesManagementController.con.getPageNumber());
        System.assertEquals('Test title', notesManagementController.title);
        System.assertEquals('Test keywords', notesManagementController.keywords);
        System.assertEquals(date.valueOf('2018-01-01'), notesManagementController.effectiveDate);
        System.assertEquals(false, notesManagementController.active);

        //More than one word in Keywords is filled
        notesManagementController.keywords = 'Test, Keyword';

        notesManagementController.searchNote();

        System.assertEquals(1, notesManagementController.con.getRecords().size());
        System.assertEquals(10, notesManagementController.con.getPageSize());
        System.assertEquals(1, notesManagementController.con.getPageNumber());
        System.assertEquals('Test title', notesManagementController.title);
        System.assertEquals('Test, Keyword', notesManagementController.keywords);
        System.assertEquals(date.valueOf('2018-01-01'), notesManagementController.effectiveDate);
        System.assertEquals(false, notesManagementController.active);
    }

    @IsTest
    static void testSearchNoteWhenAllFieldsAreFilledExceptEffectiveDate() {
        NotesManagementController notesManagementController = new NotesManagementController();

        notesManagementController.title = 'Test title';
        notesManagementController.keywords = 'Test keywords';
        notesManagementController.effectiveDate = null;
        notesManagementController.active = false;

        notesManagementController.searchNote();

        System.assertEquals(1, notesManagementController.con.getRecords().size());
        System.assertEquals(10, notesManagementController.con.getPageSize());
        System.assertEquals(1, notesManagementController.con.getPageNumber());
        System.assertEquals('Test title', notesManagementController.title);
        System.assertEquals('Test keywords', notesManagementController.keywords);
        System.assertEquals(null, notesManagementController.effectiveDate);
        System.assertEquals(false, notesManagementController.active);
    }

    @IsTest
    static void testDoDeleteSuccess() {
        NotesManagementController notesManagementController = new NotesManagementController();

        Id id = [Select Id from My_Note__c where Title__c = 'Test title'].Id;
        notesManagementController.noteId = id;

        notesManagementController.doDelete();

        List<My_Note__c> results = [Select Id from My_Note__c where Id =:id];
        System.assertEquals(0, results.size());
        System.assertEquals('Success! Your Note has been deleted.', ApexPages.getMessages()[0].getSummary());
        System.assertEquals(ApexPages.Severity.CONFIRM, ApexPages.getMessages()[0].getSeverity());
    }

    @IsTest
    static void testRefreshPageSize() {
        NotesManagementController notesManagementController = new NotesManagementController();

        //When StandardSetController is null, starting size
        notesManagementController.refreshPageSize();
        System.assertEquals(10, notesManagementController.size);

        //When StandardSetController is not null, after setting size
        setStandardSetController(notesManagementController);
        notesManagementController.size = 20;

        notesManagementController.refreshPageSize();
        System.assertEquals(20, notesManagementController.con.getPageSize());
    }

    private static void setStandardSetController(NotesManagementController controller) {
        String query = 'Select Id, Title__c, Description__c, Keywords__c, Effective_Date__c, Active__c ' +
                'from My_Note__c where Title__c = \'Test title\'';
        controller.con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
    }
}