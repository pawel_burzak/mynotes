/*
 * Created by Burzak on 2018-04-30.
 */

@IsTest
public class MyNoteBuilder {

    private My_Note__c myNote;
    private String title;
    private String description;
    private String keywords;
    private Date effectiveDate;
    private Boolean active;

    public MyNoteBuilder(){
        title = 'Test title';
        description = 'Test description';
        keywords = 'Test keywords';
        effectiveDate = date.valueOf('2018-01-01');
        active = false;
    }

    public MyNoteBuilder title(String title) {
        this.title = title;
        return this;
    }

    public MyNoteBuilder description(String description) {
        this.description = description;
        return this;
    }

    public MyNoteBuilder keywords(String keywords) {
        this.keywords = keywords;
        return this;
    }

    public MyNoteBuilder effectiveDate(Date effectiveDate) {
        this.effectiveDate = effectiveDate;
        return this;
    }

    public MyNoteBuilder active(Boolean active) {
        this.active = active;
        return this;
    }

    public MyNoteBuilder build() {
        myNote = new My_Note__c();
        myNote.Title__c = title;
        myNote.Description__c = description;
        myNote.Keywords__c = keywords;
        myNote.Effective_Date__c = effectiveDate;
        myNote.Active__c = active;
        return this;
    }

    public MyNoteBuilder save(){
        build();
        insert myNote;
        return this;
    }

    public My_Note__c getMyNote(){
        return myNote;
    }
}