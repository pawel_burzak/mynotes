/*
 * Created by Burzak on 2018-04-30.
 */

public with sharing class NotesManagementController {

    private String keywordCondition;
    private String titleCondition;
    private String query;
    public String title { get; set; }
    public String keywords { get; set; }
    public Date effectiveDate { get; set; }
    public Boolean active { get; set; }
    public Id noteId { get; set; }
    public Integer size { get; set; }
    public ApexPages.StandardSetController con { get; set; }

    public NotesManagementController() {
        myNotes = new List<My_Note__c>();
        active = false;
        size = 10;
    }

    public List<My_Note__c> myNotes {
        get {
            if (con != null) {
                return (List<My_Note__c>) con.getRecords();
            } else {
                return new List<My_Note__c>();
            }
        }
        set;
    }

    public Integer totalRecord {
        get {
            Integer result = 0;
            if (con != null) {
                result = con.getResultSize();
            }
            return result;
        }
    }

    public String pageSize {
        get {
            String result = '';
            if (con != null) {
                String firstPart = String.valueOf((con.getPageNumber() * size) + 1 - size);
                if (con.getPageNumber() * size > con.getResultSize()) {
                    result = firstPart + '-' + con.getResultSize() + ' of ' + con.getResultSize();
                } else {
                    result = firstPart + '-' + con.getPageNumber() * size + ' of ' + con.getResultSize();
                }
            } else {
                result = '0-0 of 0';
            }
            return result;
        }
    }

    public List<SelectOption> getPaginationSizeOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('5', '5'));
        options.add(new SelectOption('10', '10'));
        options.add(new SelectOption('20', '20'));
        options.add(new SelectOption('30', '30'));
        return options;
    }

    public PageReference searchNote() {
        titleCondition = String.isEmpty(title) ? '%' : title;
        keywordCondition = String.isEmpty(keywords) ? 'Keywords__c like \'%\'' : parseKeyword(keywords);

        if (effectiveDate == null) {
            query = 'Select Title__c, Description__c, Effective_Date__c, Active__c ' +
                    'from My_Note__c where Title__c LIKE :titleCondition And (' + keywordCondition +
                    ') AND Active__c = :active ORDER BY Effective_Date__c DESC NULLS LAST';
        } else {
            query = 'Select Title__c, Description__c, Effective_Date__c, Active__c ' +
                    'from My_Note__c where Title__c LIKE :titleCondition And (' + keywordCondition +
                    ') And Effective_Date__c = :effectiveDate AND Active__c = :active' +
                    ' ORDER BY Effective_Date__c DESC NULLS LAST';
        }

        con = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        con.setPageSize(size);
        return null;
    }

    private static String parseKeyword(String keywords) {
        List<String> strings = keywords.split(',');
        String result = 'Keywords__c like \'%' + String.escapeSingleQuotes(strings.get(0).trim()) + '%\'';
        strings.remove(0);

        for (String s : strings) {
            result = result + ' AND Keywords__c like \'%' + String.escapeSingleQuotes(s.trim()) + '%\'';
        }

        return result;
    }

    public PageReference doDelete() {
        try {
            My_Note__c note = [Select Id from My_Note__c where Id = :noteId];
            delete note;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Success! Your Note has been deleted.'));
        } catch (DmlException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Failure! Your Note has not been deleted.'));
            return null;
        }

        return searchNote();
    }

    public PageReference refreshPageSize() {
        if (con == null) {
            this.size = size;
        } else {
            con.setPageSize(size);
        }
        return null;
    }
}