/*
 * Created by Burzak on 2018-04-30.
 */

public with sharing class NotesGeneratorController {

    private My_Note__c myNote;

    public NotesGeneratorController() {
        myNote = new My_Note__c();
    }

    public My_Note__c getMyNote() {
        return myNote;
    }

    public PageReference save() {
        try {
            insert myNote;

            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Success! Your Note has been created.'));

            myNote = new My_Note__c();
            myNote.Title__c = null;
            myNote.Description__c = null;
            myNote.Keywords__c = null;
            myNote.Effective_Date__c = null;
            myNote.Active__c = null;

            return null;
        } catch (DmlException e) {
            ApexPages.addMessages(e);

            return null;
        }
    }
}